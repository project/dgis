--------------------------------------------------------------------------------
  2GIS Maps module Readme
  https://www.drupal.org/project/dgis
--------------------------------------------------------------------------------

Contents:
=========
1. ABOUT
2. INSTALLATION
3. REQUIREMENTS
4. CREDITS

1. ABOUT
========

Provides configurable drupal block with 2gis map
You can specify center and zoom level for each map.
Also you can add and remove markers with balloons to each map.
Also module provides new field type - dgis_maps. You can add 2GIS Maps into
your entities via this field.

2. INSTALLATION
===============

Install as usual, see https://www.drupal.org/node/895232 for further information.
Contact me with any questions.

3. REQUIREMENTS
===============

This module requires:
Module block (from Drupal core)
Module field (from Drupal core)

4. CREDITS
==========

Project page: http://drupal.org/project/dgis

Maintainers:
Eugene Ilyin - https://www.drupal.org/user/1767626
